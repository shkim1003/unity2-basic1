﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;

namespace Unity2Basic1
{
    public enum Country {Korea, China, Japan}

    public class Study1 {
        
        public void TestThread() {
            ThreadExample tExam = new ThreadExample("Hello Unity");
            ThreadStart tStart = new ThreadStart(tExam.DoWork);
            Thread thread1 = new Thread(tStart);
            thread1.Start();

            ParameterizedThreadStart tStart2 = DoWork2;
            Thread thread2 = new Thread(tStart2);
            thread2.Start("Study...");


            Thread thread3 = new Thread(
                delegate(object data)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        Console.WriteLine("{0} : {1}", data, i);
                    }
                }
            );
            thread3.Start("I wanna creating the game.");


            Thread thread4 = new Thread(
                (data) =>
                {
                    for (int i = 0; i < 10; i++) {
                        Console.WriteLine("{0} : {1}", data, i);
                    }
                }
            );
            thread4.Start("...What?");
        }

        public void DoWork2(object data) {
			for (int i = 0; i < 10; i++)
			{
				Console.WriteLine("{0} : {1}", data, i);
			}
        }
    }

   public class ThreadExample {
        string _Data = "";

        public ThreadExample(string data) {
            this._Data = data;
        }

        public void DoWork() {
            for (int i = 0; i < 10; i++) {
                Console.WriteLine("{0} : {1}", this._Data, i);
            }
        }
    }

    public class Study4 {
		public void TestDataType()
		{

			int number1 = 123;
			System.Int32 number2 = 123;

			Console.WriteLine("number1 {0}", number1);
			Console.WriteLine("number2 {0}", number2);

			double number3 = 123D;
			double number4 = 123;
			Console.WriteLine("number3 {0}", number3);
			Console.WriteLine("number4 {0}", number4);
		}


		public void TestEnum()
		{
			Country location1 = Country.Korea;
			Country location2 = (Country)1;

			int location3 = (int)location1;

			Console.WriteLine("location1 {0}", location1);
			Console.WriteLine("location2 {0}", location2);
			Console.WriteLine("location3(int) {0}", location3);
			Console.WriteLine("location3(Country) {0}", (Country)location3);
		}

		public void TestNullable()
		{
			int? number = null;
			number = 10;

			if (number.HasValue)
			{
				Console.WriteLine(number.Value);
			}
			else
			{
				Console.WriteLine("값이 null 입니다.");
			}
		}

		public void TestQuestion()
		{
			int? number = null;
			int defaultNumber1 = (number == null) ? 0 : (int)number;

			Console.WriteLine("defaultNumber1 {0}", defaultNumber1);

			int defaultNumber2 = number ?? 0;
			Console.WriteLine("defaultNumber2 {0}", defaultNumber2);
		}
    }


	public struct Employee
	{
		public int BirthDay;
		public string Name;
	}

	public class Employee2
	{
		public int BirthDay;
		public string Name;
	}
	
    public class Study5 {
        public const int ConstPrice = 10000;
        public readonly int ReadOnlyPrice;

        public Study5() {
            this.ReadOnlyPrice = 25000;
        }

        public Study5(int price) {
            this.ReadOnlyPrice = price;
        }

        public void TestStruct() {
            Employee emp1 = new Employee();
            emp1.BirthDay = 1994;
            emp1.Name = "홍길동";

            Employee emp2 = emp1;

			Console.WriteLine("emp1.BirthDay {0}", emp1.BirthDay);
            Console.WriteLine("emp2.BirthDay {0}", emp2.BirthDay);

            emp2.BirthDay = 1992;

            Console.WriteLine("<<< 생일 변경 >>>");
			Console.WriteLine("emp1.BirthDay {0}", emp1.BirthDay);
			Console.WriteLine("emp2.BirthDay {0}", emp2.BirthDay);
        }

		public void TestClass()
		{
			Employee2 emp1 = new Employee2();
			emp1.BirthDay = 1994;
			emp1.Name = "홍길동";

			Employee2 emp2 = emp1;

			Console.WriteLine("emp1.BirthDay {0}", emp1.BirthDay);
			Console.WriteLine("emp2.BirthDay {0}", emp2.BirthDay);

			emp2.BirthDay = 1992;

			Console.WriteLine("<<< 생일 변경 >>>");
			Console.WriteLine("emp1.BirthDay {0}", emp1.BirthDay);
			Console.WriteLine("emp2.BirthDay {0}", emp2.BirthDay);
		}

        public void TestDataType2() {
            string value1 = "값1";
            var value2 = "값2";
            dynamic value3 = value2;

            Console.WriteLine("value1 {0}", value1);
            Console.WriteLine("value2 {0}", value2);
            // 물어 봐야지
            //Console.WriteLine("value3 {0}", value3);
        }

        public void DisplyValue() {
            Console.WriteLine("ConstPrice = {0}", ConstPrice);
            Console.WriteLine("ReadOnlyPrice = {0}", ReadOnlyPrice);
        }

        public void ChangeCasting() {
            string numberString = "123";
            int number1 = Convert.ToInt32(numberString);

            int number2 = int.Parse(numberString);

            Console.WriteLine("number1 : {0}", number1);
            Console.WriteLine("number2 : {0}", number2);
        }
    }


    class MainClass
    {
        public static void Main(string[] args)
        {
            Study1 study1 = new Study1();
            study1.TestThread();

			Console.WriteLine("\n<<< study1 end >>>\n\n");

            Study4 study4 = new Study4();
            study4.TestEnum();
            study4.TestNullable();
            study4.TestQuestion();

            Console.WriteLine("\n<<< study4 end >>>\n\n");

            Study5 study5 = new Study5();
            study5.TestStruct();
            study5.TestDataType2();
            study5.DisplyValue();

            Study5 study51 = new Study5(600000);
            study51.DisplyValue();

            study5.ChangeCasting();
        }
    }
}
